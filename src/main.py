import sys, os, json
import pandas as pd

if __name__ == '__main__':
    sys.path.append(os.path.dirname(os.path.abspath(__file__)))

from utils.util import *
from utils.query.athena import *
from utils.athena import fetch_manifested_data, upload_file
from constants import *
from settings import *


def make_prediction(cl=None, pl=None, snm=None, rf_prd=None, pseg_scat=None, pseg_cat=None, rs=None, gm=None,
                    cl_vol=None, ctg=None, message=None):

    # TODO VALIDATE NULL CASES AND CONVERT USER ENter DATA TO LOWERCASE

    # Level0
    response_level_0 = search_level_0(cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat, rs=rs, gm=gm,
                                      cl_vol=cl_vol, ctg=ctg, message=message)

    if response_level_0['is_found']:
        return response_level_0['data']

    # Level-1
    response_level_1 = search_level_1(cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat, rs=rs, gm=gm,
                                      cl_vol=cl_vol, ctg=ctg, message=message)

    if response_level_1['is_found']:
        return response_level_1['data']

    # Level-2
    response_level_2 = search_level_2(cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat, rs=rs, gm=gm,
                                      cl_vol=cl_vol, ctg=ctg, message=message)

    if response_level_2['is_found']:
        return response_level_2['data']

    # Level-3
    response_level_3 = search_level_3(cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat, rs=rs, gm=gm,
                                      cl_vol=cl_vol, ctg=ctg, message=message)

    if response_level_3['is_found']:
        return response_level_3['data']

    # Level-4
    response_level_4 = search_level_4(cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat, rs=rs, gm=gm,
                                      cl_vol=cl_vol, ctg=ctg, message=message)

    if response_level_4['is_found']:
        return response_level_4['data']

    # Level-5
    response_level_5 = search_level_5(cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat,
                                      rs=rs, gm=gm,
                                      cl_vol=cl_vol, ctg=ctg, message=message)

    if response_level_5['is_found']:
        return response_level_5['data']

    return None


def get_manifested_data(n, from_athena=True):
    # cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat, rs=rs, gm=gm,
    #                                       cl_vol=cl_vol, ctg=ctg, message=

    if from_athena:
        fetch_manifested_data(QUERY_FETCH_MANIFESTED)
        df = pd.read_csv(PATH_TO_INPUT_ATHENA_FILE)
    else:
        df = pd.read_csv(PATH_TO_INPUT_FILE)

    if n > 0:
        df = df.head(n)

    data = df.to_json(orient='records')

    data = json.loads(data)

    p_list = []
    for i in range(len(data)):
        print(data[i])
        cl = data[i]['cl']
        pl = data[i]['pl']
        snm = data[i]['snm']
        rf_prd = data[i]['rf_prd']
        pseg_scat = data[i]['pseg_scat']
        pseg_cat = data[i]['pseg_cat']
        rs = data[i]['rs']
        gm = data[i]['gm']
        cl_vol = data[i]['cl_vol']
        ctg = data[i]['ctg']
        # message = data[i]['message']
        message = None

        predicted_result = make_prediction(cl=cl, pl=pl, snm=snm, rf_prd=rf_prd, pseg_scat=pseg_scat, pseg_cat=pseg_cat,
                                           rs=rs, gm=gm, cl_vol=cl_vol, ctg=ctg, message=message)

        if predicted_result is not None:
            output = {'wbn': data[i]['wbn'], 'actual_wt': data[i]['wt'], 'actual_vol': data[i]['vol'],
                      'wt_q_10': predicted_result['wt_q_10'], 'wt_q_90': predicted_result['wt_q_90'],
                      'wt_median': predicted_result['wt_median'], 'vol_q_10': predicted_result['vol_q_10'],
                      'vol_q_90': predicted_result['vol_q_90'], 'vol_median': predicted_result['vol_median'],
                      'prd_confidence': predicted_result['prd_confidence']}

            p_list.append(output)

    return p_list


if __name__ == '__main__':

    # Fetch data from athena/local and run prediction over it using elastic search
    p_result = get_manifested_data(MANIFESTED_COUNT, from_athena=FETCH_MANIFESTED_FROM_ATHENA)

    # Converting predicted result to dataframe
    df = convert_records_to_df(p_result)
    df.to_parquet(PATH_TO_OUTPUT_FILE)

    # Uploading data to S3 bucket as mentioned in settings.py
    upload_file(PATH_TO_OUTPUT_FILE, DUMP_FILE, BUCKET_NAME, None)

    print('\n\n')

    # Removing file to save space on server
    import os
    os.remove(PATH_TO_OUTPUT_FILE)

    print('Done !! ')