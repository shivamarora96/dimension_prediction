import os
import datetime
from settings import *

dx = datetime.date.today()

DIR_BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DIR_MODEL = DIR_BASE + '/model'

MODEL_NAME = '03_median_output.csv'
INPUT_FILE = '02_test_file_path.csv'
INPUT_ATHENA_FILE = 'manifested.csv'
OUTPUT_FILE = 'result.parquet'
DUMP_FILE = str(dx - datetime.timedelta(days=1)) + '.parquet'


PATH_TO_MODEL = DIR_MODEL + '/{}'.format(MODEL_NAME)
PATH_TO_INPUT_FILE = DIR_MODEL + '/{}'.format(INPUT_FILE)
PATH_TO_INPUT_ATHENA_FILE = DIR_MODEL + '/{}'.format(INPUT_ATHENA_FILE)

PATH_TO_OUTPUT_FILE = DIR_MODEL + '/{}'.format(OUTPUT_FILE)

BUCKET_NAME = BUCKET_NAME
ATHENA_DB = ATHENA_DB
PREFIX = ''


"""

 PUT _settings
    {
    "index": {
    "blocks": {
    "read_only_allow_delete": "false"
    }
    }
    }

"""