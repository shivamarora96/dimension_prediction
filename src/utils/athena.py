import boto3
import time
from datetime import date
from constants import *

# s3://aws-athena-query-results-190020191201-us-east-1/
bucket_name = BUCKET_NAME
db = ATHENA_DB
s3_output = 's3://' + bucket_name


def get_col_array(col_dict):
    res = []

    # array dict of cols
    input_data = col_dict['Data']

    i = 0

    while (i < len(input_data)):
        current_dict = input_data[i]
        current_col = list(current_dict.values())[0]
        res.append(current_col)
        i = i + 1

    return res


def get_current_tuple(col_data, main_data, k):
    current_tuple_array = main_data[k + 1]['Data']

    i = 0
    data = {}
    while (i < len(current_tuple_array)):
        k = col_data[i]
        v = ''

        current_data_col = current_tuple_array[i]

        if len(current_data_col) is not 0:
            v = list(current_data_col.values())[0]
        data[k] = v
        i = i + 1
    return data


# get query execution id from athena response
def get_queryid(response, logger):
    if response is None:
        print('Response is NULL -  inside get_queryid function')
        raise ValueError('Response is NULL')

    return response['QueryExecutionId']


# get query status
def get_status(client, id, logger):
    response = client.get_query_execution(
        QueryExecutionId=id
    )

    if response is None:
        print('Response is NULL for get status')
        raise ValueError('Response is NULL for get status')

    data_state = response['QueryExecution']
    state = data_state['Status']['State']
    print('state - ' + state)
    # print('state', state)
    return state


def download(file, logger, file_name=None):
    try:

        sts_client = boto3.client('sts')
        assumedRoleObject = sts_client.assume_role(RoleArn="arn:aws:iam::190020191201:role/stsDataServices",
                                                   RoleSessionName="SessionRole")
        credentials = assumedRoleObject['Credentials']

        session = boto3.Session(
            region_name='us-east-1', aws_access_key_id=credentials['AccessKeyId'],
            aws_secret_access_key=credentials['SecretAccessKey'],
            aws_session_token=credentials['SessionToken'])

        s3 = session.resource('s3')
        bucket = s3.Bucket(bucket_name)
        bucket.download_file(file, PREFIX + file_name)

    except Exception as e:
        raise Exception('Failed to download file -' + str(e))


# get query data from athena
def get_data(Query=None):
    try:

        if Query is None:
            raise ValueError('Nil Query is passed to the athena')

        # create clinet
        client = boto3.client('athena', region_name='us-east-1')

        response = client.start_query_execution(QueryString=Query,
                                                ResultConfiguration={
                                                    'OutputLocation': s3_output
                                                }
                                                )

        query_id = get_queryid(response)

        print('\n\nQuerying Data Please Wait ...\n\nid:', query_id)

        while True:
            status = get_status(client, query_id)

            print('current status - ' + status)

            QUERY_STATE_QUEUED = 'QUEUED'
            QUERY_STATE_RUNNING = 'RUNNING'
            QUERY_STATE_SUCCEEDED = 'SUCCEEDED'
            QUERY_STATE_FAILED = 'FAILED'
            QUERY_STATE_CANCELLED = 'CANCELLED'

            if status == QUERY_STATE_SUCCEEDED:
                print('\nS\n')
                result = []
                temp = client.get_query_results(QueryExecutionId=query_id)
                col_data = get_col_array(temp['ResultSet']['Rows'][0])
                for k in range(1, len(temp['ResultSet']['Rows'])):
                    current_tuple = get_current_tuple(col_data, temp['ResultSet']['Rows'], k - 1)
                    result.append(current_tuple)

                return result

            if status == QUERY_STATE_FAILED or status == QUERY_STATE_CANCELLED:
                print('\nF\n')
                result = []
                return result

            time.sleep(3)
        return []

    except Exception as e:
        print('\nE\n', str(e))
        return []


def get_file(logger, Query=None, file_name='demo1'):
    try:

        if Query is None:
            print('Nil Query is passed to the athena -  in get_file function')
            # logger.info('Nil Query is passed to the athena')
            raise ValueError('Nil Query is passed to the athena')

        sts_client = boto3.client('sts')
        assumedRoleObject = sts_client.assume_role(RoleArn="arn:aws:iam::190020191201:role/stsDataServices",
                                                   RoleSessionName="SessionRole")
        credentials = assumedRoleObject['Credentials']

        client = boto3.client('athena', region_name='us-east-1', aws_access_key_id=credentials['AccessKeyId'],
                              aws_secret_access_key=credentials['SecretAccessKey'],
                              aws_session_token=credentials['SessionToken'])

        # create clinet
        # client = boto3.client('athena', region_name='us-east-1')

        response = client.start_query_execution(QueryString=Query,
                                                ResultConfiguration={
                                                    'OutputLocation': s3_output
                                                }
                                                )

        query_id = get_queryid(response, logger)

        file = query_id + '.csv'

        # logger.info('Querying Data Please Wait... id:{}'.format(query_id))
        print('Querying Data Please Wait... id:{}'.format(query_id))

        while True:
            status = get_status(client, query_id, logger)

            print('current status - ' + status)

            QUERY_STATE_QUEUED = 'QUEUED'
            QUERY_STATE_RUNNING = 'RUNNING'
            QUERY_STATE_SUCCEEDED = 'SUCCEEDED'
            QUERY_STATE_FAILED = 'FAILED'
            QUERY_STATE_CANCELLED = 'CANCELLED'

            if status == QUERY_STATE_SUCCEEDED:
                print('success')
                # logger.info('success')
                # result = []
                # temp = client.get_query_results(QueryExecutionId=query_id)
                # col_data = get_col_array(temp['ResultSet']['Rows'][0])
                # for k in range(1, len(temp['ResultSet']['Rows'])):
                #     current_tuple = get_current_tuple(col_data, temp['ResultSet']['Rows'], k - 1)
                #     result.append(current_tuple)
                time.sleep(5)
                print('Downloading file...')
                # logger.info('Downloading file...')
                download(file, logger, file_name=file_name)
                print('Download Completed !!  - ' + file_name)
                # logger.info('Downloading Completed\n\n')
                return file_name

            if status == QUERY_STATE_FAILED or status == QUERY_STATE_CANCELLED:
                print('\nF\n')
                result = []
                print('Download Failed !')
                # logger.info('Downloading Failed\n\n')
                return None

            time.sleep(3)
        return None

    except Exception as e:
        print('Download Failed !' + 'Error - ' + str(e))
        # logger.info('Downloading Failed - Error - ' + str(e) +  '\n\n')
        return None


def upload(file, cat, logger):
    try:
        print('stating uploading of ' + file + ' to ' + bucket_name)
        bucketName = bucket_name
        Key = file
        prefix = PREFIX + cat + "_"
        outPutname = prefix + str(date.today()) + '.parquet'
        s3 = boto3.client('s3')
        s3.upload_file(Key, bucketName, 'data_glue/{}'.format(outPutname))
        print('Uploaded Successfully')
    except Exception as e:
        print('File upload failed - ' + str(e))


def upload_file(file, name, bucket_name, logger):
    try:
        print('stating uploading of ' + file + ' to ' + bucket_name + ' as ' + name)
        # logger.info('stating uploading of ' + file + ' to ' + bucket_name + ' as ' + name)
        bucketName = bucket_name
        Key = file
        outPutname = name

        sts_client = boto3.client('sts')
        assumedRoleObject = sts_client.assume_role(RoleArn="arn:aws:iam::190020191201:role/stsDataServices",
                                                   RoleSessionName="SessionRole")
        credentials = assumedRoleObject['Credentials']

        s3 = boto3.client('s3', region_name='us-east-1', aws_access_key_id=credentials['AccessKeyId'],
                          aws_secret_access_key=credentials['SecretAccessKey'],
                          aws_session_token=credentials['SessionToken'])

        # s3 = boto3.client('s3')
        s3.upload_file(Key, bucketName, 'dp_dumps/{}'.format(outPutname))
        print('Uploaded Successfully')
        # logger.info('Uploaded Successfully\n')
    except Exception as e:
        # logger.info('File upload failed - ' + str(e) + '\n')
        print('File upload failed - ' + str(e))



def fetch_manifested_data(query):
    check = get_file(None, Query=query, file_name=PATH_TO_INPUT_ATHENA_FILE)
    if check is None:
        return False
    return True
