import sys, os, pandas as pd

from utils.query.es import *
from utils.elastic_search import ElasticSearch


def convert_records_to_df(list_of_dicts):

    if list_of_dicts is None or len(list_of_dicts) < 1:
        raise ValueError('Either list of data is Empty or Null while converting to DF')

    df = pd.DataFrame.from_records(list_of_dicts)

    print('converted Df : \n')
    print(df.to_string())

    return df


def validate_input(cl, pl, snm, rf_prd, pseg_scat, pseg_cat, rs, gm, cl_vol, ctg, message):
    if cl is None:
        raise ValueError('client is Null')
    if pl is None:
        raise ValueError('Pickup Location is Null')
    if snm is None:
        raise ValueError('seller name is Null')
    if rf_prd is None:
        raise ValueError('rf_prd is Null')
    if pseg_scat is None:
        raise ValueError('pseg_scat is Null')
    if pseg_cat is None:
        raise ValueError('pseg_cat is Null')
    if rs is None:
        raise ValueError('rs is Null')
    if gm is None:
        raise ValueError('gm is Null')
    if cl_vol is None:
        raise ValueError('cl_vol is Null')
    if ctg is None:
        raise ValueError('ctg is Null')
    if message is None:
        raise ValueError('message is Null')

    return True


# cl-pl-snm-rf_prd-pseg_scat-rs-gm-cl_vo
def search_level_0(cl=None, pl=None, snm=None, rf_prd=None, pseg_scat=None, pseg_cat=None, rs=None, gm=None,
                    cl_vol=None, ctg=None, message=None, Query=query_level_0):

    temp_query = Query.replace('\n', '')
    temp_query = temp_query.replace('\t', '')
    temp_query = temp_query.replace('{}', 'openclose')
    temp_query = temp_query.replace('{', 'open$')
    temp_query = temp_query.replace('}', 'close$')
    temp_query = temp_query.replace('openclose', '{}')
    temp_query = str(temp_query).format(cl, pl, snm, rf_prd, pseg_scat, rs, gm, cl_vol)
    temp_query = temp_query.replace('close$', '}')
    temp_query = temp_query.replace('open$', '{')

    print(temp_query)
    return ElasticSearch.get_result(temp_query)


# cl-pl-snm-rf_prd-pseg_cat-rs-gm-cl_vo
def search_level_1(cl=None, pl=None, snm=None, rf_prd=None, pseg_scat=None, pseg_cat=None, rs=None, gm=None,
                    cl_vol=None, ctg=None, message=None, Query=query_level_1):

    temp_query = Query.replace('\n', '')
    temp_query = temp_query.replace('\t', '')
    temp_query = temp_query.replace('{}', 'openclose')
    temp_query = temp_query.replace('{', 'open$')
    temp_query = temp_query.replace('}', 'close$')
    temp_query = temp_query.replace('openclose', '{}')
    temp_query = str(temp_query).format(cl, pl, snm, rf_prd, pseg_cat, rs, gm, cl_vol)
    temp_query = temp_query.replace('close$', '}')
    temp_query = temp_query.replace('open$', '{')

    print(temp_query)
    return ElasticSearch.get_result(temp_query)


# cl-pl-snm-rf_prd-ctg-rs-gm-cl_vol
def search_level_2(cl=None, pl=None, snm=None, rf_prd=None, pseg_scat=None, pseg_cat=None, rs=None, gm=None,
                    cl_vol=None, ctg=None, message=None, Query=query_level_2):

    temp_query = Query.replace('\n', '')
    temp_query = temp_query.replace('\t', '')
    temp_query = temp_query.replace('{}', 'openclose')
    temp_query = temp_query.replace('{', 'open$')
    temp_query = temp_query.replace('}', 'close$')
    temp_query = temp_query.replace('openclose', '{}')
    temp_query = str(temp_query).format(cl, pl, snm, rf_prd, ctg, rs, gm, cl_vol)
    temp_query = temp_query.replace('close$', '}')
    temp_query = temp_query.replace('open$', '{')

    print(temp_query)
    return ElasticSearch.get_result(temp_query)


# cl-pl-snm-rf_prd-rs-gm-cl_vol
def search_level_3(cl=None, pl=None, snm=None, rf_prd=None, pseg_scat=None, pseg_cat=None, rs=None, gm=None,
                    cl_vol=None, ctg=None, message=None, Query=query_level_3):

    temp_query = Query.replace('\n', '')
    temp_query = temp_query.replace('\t', '')
    temp_query = temp_query.replace('{}', 'openclose')
    temp_query = temp_query.replace('{', 'open$')
    temp_query = temp_query.replace('}', 'close$')
    temp_query = temp_query.replace('openclose', '{}')
    temp_query = str(temp_query).format(cl, pl, snm, rf_prd, rs, gm, cl_vol)
    temp_query = temp_query.replace('close$', '}')
    temp_query = temp_query.replace('open$', '{')

    print(temp_query)
    return ElasticSearch.get_result(temp_query)


#cl-pl-rf_prd-rs-gm-cl_vol
def search_level_4(cl=None, pl=None, snm=None, rf_prd=None, pseg_scat=None, pseg_cat=None, rs=None, gm=None,
                    cl_vol=None, ctg=None, message=None, Query=query_level_4):

    temp_query = Query.replace('\n', '')
    temp_query = temp_query.replace('\t', '')
    temp_query = temp_query.replace('{}', 'openclose')
    temp_query = temp_query.replace('{', 'open$')
    temp_query = temp_query.replace('}', 'close$')
    temp_query = temp_query.replace('openclose', '{}')
    temp_query = str(temp_query).format(cl, pl, rf_prd, rs, gm, cl_vol)
    temp_query = temp_query.replace('close$', '}')
    temp_query = temp_query.replace('open$', '{')

    print(temp_query)
    return ElasticSearch.get_result(temp_query)


#cl-pl-snm-rf_prd-rs-gm
def search_level_5(cl=None, pl=None, snm=None, rf_prd=None, pseg_scat=None, pseg_cat=None, rs=None, gm=None,
                    cl_vol=None, ctg=None, message=None, Query=query_level_5):

    temp_query = Query.replace('\n', '')
    temp_query = temp_query.replace('\t', '')
    temp_query = temp_query.replace('{}', 'openclose')
    temp_query = temp_query.replace('{', 'open$')
    temp_query = temp_query.replace('}', 'close$')
    temp_query = temp_query.replace('openclose', '{}')
    temp_query = str(temp_query).format(cl, pl,snm, rf_prd, rs, gm)
    temp_query = temp_query.replace('close$', '}')
    temp_query = temp_query.replace('open$', '{')

    print(temp_query)
    return ElasticSearch.get_result(temp_query)