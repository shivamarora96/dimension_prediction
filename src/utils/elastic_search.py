import urllib3, os, sys
import json
from utils.connectivity import check_connection


class ElasticSearch():
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    __host__ = 'http://localhost:9200'
    __ind__ = 'trained'
    __type__ = '_doc'
    __http__ = urllib3.PoolManager()

    @staticmethod
    @check_connection(HTTP_CLIENT=__http__, ES_URL=__host__)
    def get_data_file(query_file, es_host=__host__, index=__ind__, type=__type__):

        path_to_json = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))) + '/json/' + query_file

        with open(path_to_json) as json_file:
            data = json.load(json_file)
            encode_body = json.dumps(data)
            url = es_host + '/' + index + '/' + type + '/_search'
            response = ElasticSearch.__http__.request(ElasticSearch.GET, url,  headers={'Content-Type': 'application/json'}, body=encode_body)
            return response.data


    @staticmethod
    @check_connection(HTTP_CLIENT=__http__, ES_URL=__host__)
    def get_result(query, es_host=__host__, index=__ind__, type=__type__, method=GET):

        try:
            url = es_host + '/' + index + '/' + type + '/_search'
            response = ElasticSearch.__http__.request(method, url,  headers={'Content-Type': 'application/json'}, body=query)
            encode_response = response.data.decode('utf-8')
            result = json.loads(encode_response)['hits']

            output = {'is_error': False if str(response.status)[0] == '2' else True,
                      'is_found': True if result['total']['value'] > 0 else False,
                       'count':  result['total']['value'],
                       'data': None if result['total']['value'] == 0 or str(response.status)[0] != '2' else result['hits'][0]['_source']
                      }

            return output

        except Exception as e:

            output = {'is_error': True,
                      'is_found': False,
                      'count': 0,
                      'data': str(e)
                      }

            return output

